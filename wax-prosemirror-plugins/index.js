export { default as TrackChangePlugin } from './src/trackChanges/TrackChangePlugin';

export { default as CommentPlugin } from './src/comments/CommentPlugin';
export { default as WaxSelectionPlugin } from './src/WaxSelectionPlugin';
export { default as highlightPlugin } from './src/highlightPlugin';
