import { DOMSerializer } from "prosemirror-model";

class WaxDOMSerializer extends DOMSerializer {}

export default WaxDOMSerializer;
