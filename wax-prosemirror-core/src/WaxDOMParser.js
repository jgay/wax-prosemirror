import { DOMParser } from "prosemirror-model";

class WaxDOMParser extends DOMParser {}

export default WaxDOMParser;
