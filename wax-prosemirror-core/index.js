export { WaxContext, useInjection } from "./src/WaxContext";
export { default as Wax } from "./src/Wax";
